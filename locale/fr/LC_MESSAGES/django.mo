��    `       �        �     �  &   �     �     �     �          $  	   -     7  
   R     ]     f     u     �  
   �     �  	   �  
   �     �     �     �  <   �  =   +     i  7   o  &   �  '   �     �              	   4      >      C      I      `   	   g      q   
   z      �      �      �      �      �   *   �   .   �      $!     *!  S   ;!  
   �!     �!  	   �!     �!     �!     �!     �!     �!     "  "   "     <"     H"     a"     p"     x"     }"     �"     �"     �"     �"     �"     �"     �"     �"     �"     �"     #     #     '#     4#  	   =#     G#  1   N#  (   �#  %   �#  !   �#  ,   �#     $     5$     I$     Z$     t$     {$     �$     �$     �$  $   �$     �$  �   �$  �   �%  �   �&  h   8'  &  �'  0  �(     �)  	   �)     *     *  %   )*      O*     p*     �*  5   �*  �   �*  q   ^+  �   �+  �   T,  9   -  �   W-     �-     �-     .     !.     3.     C.     I.     O.  	   `.     j.  	   s.     }.     �.     �.  	   �.  	   �.     �.     �.     �.     �.  	   �.  !   /  
   #/     ./     3/     @/     M/  C   Z/     �/     �/     �/     �/     �/     �/  	   �/     0  	   0     0     &0     /0  	   80  
   B0     M0     Y0     f0     l0     s0     �0     �0     �0     �0     �0  $   �0     �0     �0     1  "   1     81  "   U1     x1     �1     �1     �1     �1  
   2     2     2     #2     A2     O2     W2     ^2     j2  9   ~2     �2     �2     �2     �2     �2     �2     �2     3  	   -3     73     G3     T3     j3     o3  d   ~3     �3     �3  *   4     :4     Z4     `4  "   z4  !   �4  !   �4     �4     �4     5     .5  ,   A5  !   n5  B   �5     �5     �5     �5  
   �5     6     6     6     6     (6     /6     A6     H6     L6     Q6  
   _6  
   j6     u6  !   {6  K   �6  m   �6     W7  (   m7     �7  *   �7     �7     �7  (   8  +   ;8  0   g8  )   �8  4   �8  1   �8     )9     29     89  ?   ?9  *   9  ,   �9  U   �9  B   -:  W   p:  D   �:     ;     ;     ;     !;     );     2;     D;     _;     c;     u;     {;     �;     �;  	   �;  
   �;  "   �;  "   �;     <     <     -<     G<     O<  "   d<  o   �<     �<     �<     =     !=     *=  6   6=  6   m=  '   �=  W   �=  L   $>  [   q>  2   �>  	    ?     
?  :   ?     V?  1   e?  E   �?  Y   �?  I   7@  =   �@  +   �@  '   �@  -   A  4   AA  )   vA  :   �A  8   �A  9   B  @   NB  9   �B  *   �B     �B  -   �B     +C     9C     =C     FC  	   IC  	   SC     ]C     nC     wC     �C     �C     �C     �C  $   �C     �C  )   �C     D     %D  )   +D     UD     \D     pD     xD     �D  	   �D     �D     �D  "   �D     �D  !  �D  %   �E  ,   
F     7F     @F     ZF  $   rF     �F  
   �F     �F  
   �F  	   �F     �F     �F     G     .G     @G     WG     nG     �G     �G     �G  J   �G  7   H  	   =H  ;   GH  @   �H  @   �H     I     I     5I  
   MI     XI     ^I     fI     }I     �I  	   �I     �I  
   �I     �I     �I  #   �I     J  3   J  5   AJ     wJ     J  j   �J     K     K  	   %K     /K     @K  	   WK     aK  	   zK     �K  %   �K     �K      �K     �K     �K     L     L     !L     4L     OL     kL     �L     �L     �L     �L     �L     �L     �L     �L     M     M  
   M     (M  @   0M  4   qM  )   �M  '   �M  R   �M  &   KN  "   rN     �N     �N  	   �N     �N     �N  &   �N     O  3   !O     UO    aO    �P  �   �Q  �   5R  m  �R  e  IT     �U     �U     �U     �U  +   �U  $   %V     JV  +   YV  ?   �V  �   �V  {   [W  �   �W  �   xX  ;   LY  �   �Y     LZ  .   YZ  :   �Z     �Z     �Z     �Z  
   [     [     +[     4[  
   =[     H[     U[     b[  
   k[     v[     [     �[     �[     �[  
   �[  '   �[  
   \     \     \     )\     =\  `   R\     �\     �\     �\     �\     ]     ]     -]     :]     N]     \]     k]     ~]     �]     �]     �]     �]     �]     �]     �]     �]     ^  
   ^     ^     8^     M^     k^     w^     �^  (   �^     �^  B   �^  &   2_  4   Y_  0   �_  3   �_  *   �_     `     ,`     4`     ;`     Y`     o`  	   w`     �`     �`  9   �`     �`     �`     �`     a  	   a     a      <a     ]a  
   }a     �a     �a     �a     �a     �a  h   �a     Ib  '   [b  9   �b  '   �b  	   �b  #   �b  /   c  5   Cc  0   yc     �c     �c  $   �c     
d  1    d  /   Rd  D   �d     �d     �d     �d     �d     e     e     e     %e     -e     5e     Oe  
   We     be     ne     �e     �e     �e      �e  e   �e  �   Cf     �f  .   �f     g  7   $g     \g     |g  +   �g  +   �g  0   �g  6   $h  <   [h  8   �h     �h     �h     �h  ]   �h  C   Fi  C   �i  r   �i  [   Aj  w   �j  Z   k     pk     xk     �k     �k  	   �k     �k      �k     �k     �k     �k     �k     l     'l  
   @l     Kl  ,   Wl  -   �l     �l     �l     �l  
   �l     �l  .   �l  �   &m     �m  	   �m     �m     �m     �m  <   n  >   Dn  0   �n  b   �n  O   o  o   go  @   �o  	   p     "p  4   3p     hp  2   yp  N   �p  l   �p  N   hq  :   �q  +   �q  !   r  7   @r  C   xr  (   �r  <   �r  >   "s  ?   as  Q   �s  @   �s  1   4t     ft  0   ot     �t     �t  	   �t     �t     �t  	   �t     �t  	   �t     �t     �t     u     #u     /u  +   ;u     gu  1   �u  
   �u     �u  ,   �u  	   �u     �u     v     v     v  
   #v     .v     3v  (   8v     av     �     �   �   �   D      	   A     q         �       �   :   �   �   �   X  _  |     &       �       w   �   �   �   �     Y  1       9      �   �         3         �                    �   -       I   ?   .       �       %  H       .  �       �   �   e   �   T  -      :  �   P           F   M  ,   �   `  '      R   &  �   4      J              E             7   +  6      M       �   @       \   �   i   �   3   �     �   �   C   �   H     �       �       �       U   �               �   �   �         �       !               �   �   �   j   X   G  2  Z   <  �   �   >   �       �   5   V       P  +           �       p       �   6   �      <   v       D   /  �               T       Y   V  8   f   Q  n   J             �   �       �   �       �   g   ^  ]          U  �   \      �   ]  �      7  �       d   �          =   1                 B               �   �   �         z   N  �       �             �         �   %   �           L  >  �   
  4            R          k          �   �   �     ,              W   t           �       [  )   �      2   G   @  �   A  �   �   �   �   �       �   ~   {   r   }       Q              s   S                     �   ;  l   �   �       �   /           �   �   �   ^   y       b   �     	     �   �       �   O      x         O   E      �       �   m   �   �   
   *         ;       �   �   `       $      !  #  (        L   �   N             �   u   �   �   "     �           �       $   a       �   �                 �   F  #   �   �       �           [   *        h   Z  �           0           �   =        o   �     0  )    c           5      �   K  �      �   W                 �       �   �   C  _   �            �   �   �   I             "  B  �   S   9          ?             '   8  (  K       �   �    %(count)d mails have been sent. A user with that email already exists. Accept Accept talk? Accept the proposal Acceptances disclosure date Accepted Accepted! Access an existing profile Activities Activity Add a category Add a co-speaker Add a new user Add a room Add a speaker Add a tag Add a talk Add a track Add an activity Administration An email has been sent with a link to access to your profil. An user with that firstname and that lastname already exists. Apply Are you really sure that you want to delete a speaker ? Are you sure to accept this proposals? Are you sure to decline this proposals? Assign to a track Become a volunteer! Beginning date and time Biography Body Body: Call for participation Cancel Cancelled Capacity Categories Category Change password Changes saved. Check template validity City Co-speaker successfully added to the talk. Co-speaker successfully removed from the talk. Color Color on program Comment this talk – <em>this message will be received by the staff team only</em> Conference Conference name Confirmed Contact email Decision taken in account Decline Decline the proposal Declined Default duration (min) Default duration: %(duration)d min Description Description of your talk Duration (min) E-mail: Edit Edit a speaker Edit a talk Edit activity Edit category Edit tag Edit this proposal Edit your profile Editor Email Email address Email: End of the conference date English Environment: Facebook Facebook: Filter Filter talks already / not yet affected to a room Filter talks already / not yet scheduled Filter talks with / without materials Filter talks with / without video Filter talks you already / not yet voted for For selected speakers: For selected talks: Forgot password? Format '%s' not available French Github Github: Go back to proposals list Go back to the talk Good news, I finally could be there! Goodbye! Hi %(name)s,

Someone, probably you, asked to access your volunteer profile.
You can update your availabilities or edit your profile following this url:

  %(url)s

If you have any question, your can answer to this email.

Sincerely,

%(footer)s
 Hi {},

Someone, probably you, asked to access your profile.
You can edit your talks or add new ones following this url:

  {}

If you have any question, your can answer to this email.

Sincerely,

{}

 Hi {},

Thank your for your help in the organization of the conference {}!

You can update your availability at anytime:

    {}

Thanks!

{}

 Hi {},

You have been added to the staff team.

You can now:
- login: {}
- reset your password: {}

{}

 Hi {},

Your talk has been submitted for {}.

Here are the details of your talk:
Title: {}
Description: {}

You can at anytime:
- review and edit your profile: {}
- review and edit your talk: {}
- add a new co-speaker: {}

If you have any question, your can answer to this email.

Thanks!

{}

 Hi {},

{} add you as a co-speaker for the conference {}.

Here is a summary of the talk:
Title: {}
Description: {}

You can at anytime:
- review and edit your profile: {}
- review and edit the talk: {}
- add another co-speaker: {}

If you have any question, your can answer to this email.

Thanks!

{}

 Home Home page Homepage (markdown) I need sound I read my self twice, confirm sending I will be happy to help on that! I will be there! I'm ok to be recorded on video If specified, schedule tab will redirect to this URL. If you already have submitted a talk and you want to edit it or submit another one, please click <a href="%(mail_token_url)s">here</a>. If you already have submitted a talk and you want to edit it, please click <a href="%(mail_token_url)s">here</a>. If you already subscribe as a volunteer and want to update your availabilities, please click <a href="%(mail_token_url)s">here</a>. If you have any constraint or if you have anything that may help you to select your talk, like a video or slides of your talk, please write it down here. This field will only be visible by organizers. If you have some constraints, you can indicate them here. If you want to send a message to the proposer, please enter it below. Remember to indicate which talk your message is reffering. Information Information about the proposals Information for the proposer Intervention kind Invited speaker Kind: Label Label on program Language: LinkedIn LinkedIn: Login Logout Mastodon Mastodon: Materials Materials and video Message from %(author)s Message sent! Message to organizers Messaging Modifications successfully saved. My profile Name New activity New category New proposal New staff members will be informed of their new position by e-mail. New tag No activites. No biography. No categories. No description provided. No messages. No notes. No proposals. No rooms. No speakers. No tags. No talks No talks. No tracks. Nope, Abort Not assigned Notes Notes: Notify by mail? Okay, no problem! Organisation Participate Password Change Pending decision Pending decision, score: %(score).1f Phone Phone number Phone number: Please confirm your participation: Please correct those errors. Please login to access staff area. Please select a category. Please select some speakers. Please select some talks. Please select some volunteers. Please write your email bellow: Powered by Preview Profile Profile updated successfully. Put in a room Refused Remove Reply email Reset your password Reviewing in progress, we will keep you informed by mail. Room Rooms SMS prefered Save Schedule Schedule evicted from cache. Schedule publishing date Schedule redirection URL Scheduled Scheduled talks Secret link: Secure domain (HTTPS) Send Send a message Send a message – <em>this message will be received by this participant and all the staff team</em> Send an email Send an email to each speaker Send an email to each speaker of each talk Send an email to each volunteer Send! Show filtering options… Show the tag on the public program Show the tag on the staff program Some talks are not scheduled yet. Sorry, I have a setback Sorry, I have to cancel. Sorry, couldn't make it :-( Sorry, refused :-( Sorry, the Call for Participation is closed! Sorry, we do not know this email. Speaker %(speaker)s %(action)s his/her participation for %(talk)s. Speaker confirmation Speakers Staff Staff area Staff members Status Subject Subject: Submit Submit a proposal Syntax Tag Tags Talk Category Talk Title Talk count Talks Thank you for your participation! Thank you for your participation! You can now subscribe to some activities. The reply email should be a formatable string accepting a token argument (e.g. ponyconf+{token}@exemple.com). The speaker cancelled The speaker confirmation has been noted. The speaker confirmed The speaker unavailability has been noted. The talk has been %(action)s. The talk has been confirmed. There is an error in your body template. There is an error in your subject template. This description will be visible on the program. This field is only visible by organizers. This volunteer applied for the following activities: This volunteer hasn't applied for any activities. Timeslot Title Title: To preview your email, click on a speaker and talk combination: To preview your email, click on a speaker: To preview your email, click on a volunteer: To receive a email with a link to access your profile, please enter your email below. To see available environment variables, please click on a speaker. To see available environment variables, please click on a talk and speaker combination. To see available environment variables, please click on a volunteer. Total: Track Tracks Twitter Twitter: Unscheduled talks User created successfully. VIP Venue information Video Video licence Video publishing date Visible by speakers Volunteer Volunteers Volunteers enrollment closing date Volunteers enrollment opening date Vote Vote successfully created Vote successfully updated Waiting Waiting confirmation We have noted your unavailability. We need you! To participate, please enter your name and e-mail and select the activities you are interested in. Website Website: Welcome <b>%(name)s</b>! Welcome! Yes, Delete You already cancelled your participation to this talk. You already confirmed your participation to this talk. You are ready to send %(count)d emails. You can use <a href="http://jinja.pocoo.org/docs/2.10/">Jinja2</a> templating language. You can use this field to share some materials related to your intervention. You have a pending e-mail. To continue its edition, click <a href="%(email_url)s">here</a>. You may want to add one of the following speakers: Your Name Your information Your participation has been taken into account, thank you! Your proposals Your proposition has been successfully submitted! Your template does not compile (at least) with speaker '%(speaker)s'. Your template does not compile (at least) with talk '%(talk)s' and speaker '%(speaker)s'. Your template does not compile (at least) with volunteer '%(volunteer)s'. [%(conference)s] %(speaker)s %(action)s his/her participation [%(conference)s] Conversation with %(user)s [%(conference)s] Message from the staff [%(conference)s] New comment about '%(talk)s' [%(conference)s] Someone asked to access your profil [%(conference)s] Thank you for your help! [%(conference)s] Thank you for your proposition '%(talk)s' [%(conference)s] The talk '%(talk)s' has been %(action)s [%(conference)s] The talk '%(talk)s' has been %(action)s. [%(conference)s] You have been added as co-speaker to '%(talk)s' [%(conference)s] Your talk '%(talk)s' has been %(action)s [{}] You have been added to the staff team accepted accepted: %(accepted)s accepted: %(accepted)s activityNone and average: by cancelled confirmed contact by email declined download download as csv in manager not defined one per speaker and talk combination only one per speaker pending: %(pending)s pending: %(pending)s phone numbernot provided place refused: %(refused)s refused: %(refused)s remove sessionnot defined speaker tagnone talk volunteer vote with you must confirm you participation you! Project-Id-Version: 
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2019-11-02 13:06+0100
Last-Translator: 
Language-Team: 
Language: fr
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=(n > 1);
X-Generator: Poedit 2.2.4
 %(count)d e-mails ont été envoyés. Un utilisateur avec cet email existe déjà. Accepter Accepter la proposition ? Accepter la proposition Date de divulgation des acceptations Accepté Accepté ! Accéder à un profil existant Activités Activité Ajouter une catégorie Ajouter un co-intervenant Ajouter un nouvel utilisateur Ajouter une salle Ajouter un intervenant Ajouter une étiquette Ajouter un exposé Ajouter une session Ajouter une activité Administration Un e-mail vous a été envoyé avec un lien pour accéder à votre profil. Un utilisateur avec ce prénom et ce nom existe déjà. Appliquer Êtes vous vraiment sûr de vouloir supprimer cet orateur ? Êtes-vous sûr d’accepter cette propositon d’intervention ? Êtes-vous sûr de décliner cette propositon d’intervention ? Assigner à une session Devenez un bénévole ! Date et heure de début Biographie Corps Corps : Appel à participation Annuler Annulé Capacité Catégories Catégorie Changer de mot de passe Modifications sauvegardées. Vérifier la validité des gabarits Ville Co-intervenant ajouté à l’exposé avec succès. Co-intervenant supprimé de l’exposé avec succès. Couleur Couleur sur le programme Commenter cette proposition – <em>ce message sera reçu uniquement par l’équipe d’organisation</em> Conférence Nom de la conférence Confirmé Email de contact Décision enregistrée Décliner Décliner la proposition Décliné Durée par défaut (min) Durée par défaut : %(duration)d min Description Description de votre proposition Durée (min) E-mail : Éditer Éditer un orateur Éditer un exposé Édition d’une activité Édition d’une catégorie Édition d’une étiquette Éditer cette proposition Éditer votre profil Éditeur E-mail Adresse e-mail E-mail : Date de fin de la conférence Anglais Environnement : Facebook Facebook : Filtrer Filtrer les exposés déjà / pas encore affectées à une salle Filtrer les exposés déjà / pas encore planifiées Filtrer les exposés avec / sans supports Filtrer les exposés avec / sans vidéo Filtrer les propositions pour lesquelles vous avez déjà voté / pas encore voté Pour les intervenants sélectionnés : Pour les exposés sélectionnés : Mot de passe oublié ? Format '%s' non disponible Français Github Github : Retourner à la liste des propositions Retourner à l’exposé Bonne nouvelle, je peux finalement être présent ! Au revoir ! Bonjour %(name)s,

Quelqu’un, sans doute vous, a demandé à accéder à votre profil bénévole.
Vous pouvez modifier vos disponibilité ou éditer votre profil à l’url suivante :

  %(url)s

Si vous avez une question, vous pouvez répondre à ce mail.

Sincèrement,


%(footer)s
 Bonjour {},

Quelqu’un, sans doute vous, a demandé à accéder à votre profil.
Vous pouvez modifier vos propositions ou en soumettre de nouvelles à l’url suivante :

  {}

Si vous avez une question, vous pouvez répondre à ce mail.

Sincèrement,

{}

 Bonjour {},

Merci pour votre aide pour l'organisation de la conférence {} !

Vous pouvez mettre à jour vos disponibilité à n'importe quel moment :

    {}

Merci !

{}

 Bonjour {},

Vous avez été ajouté à l’équipe d’organisation.

Vous pouvez à présent :
- vous connecter : {}
- réinitialiser votre mot de passe : {}

{}

 Bonjour {},

Votre proposition a été transmise à {}.

Vous trouverez ci-dessous les détails de votre proposition:
Titre: {}
Description: {}

Vous pouvez à tout moment:
- consulter et modifier votre profil : {}
- consulter et modifier votre proposition : {}
- ajouter un autre intervenant : {}

Si vous avez une question, vous pouvez répondre à ce mail.

{}

 Bonjour {},

{} vous a ajouté comme co-intervenant lors de la conférence {}.

Voici un résumé de l’exposé :
Titre: {}
Description: {}

Vous pouvez à tout moment :
- consulter et modifier votre profil : {}
- consulter et modifier l’exposé : {}
- ajouter un autre intervenant : {}

Si vous avez une question, vous pouvez répondre à ce mail.

{}

 Accueil Page d’accueil Page d’accueil (markdown) J’ai besoin de son Je me suis relu 2 fois, confirmer l’envoi Je serai heureux d’aider à cela ! Je serai là ! J’accepte d’être enregistré en vidéo Si spécifiée, l’onglet programme redirigera vers cette URL. Si vous avez déjà soumis une proposition et que vous souhaitez l’éditer ou en soumettre une autre, cliquez <a href="%(mail_token_url)s">ici</a>. Si vous avez déjà soumis une proposition et que vous souhaitez l’éditer, cliquez <a href="%(mail_token_url)s">ici</a>. Si vous vous êtes déjà inscrit en tant que bénévole et que vous souhaitez mettre à jour vos disponibilités, cliquez <a href="%(mail_token_url)s">ici</a>. Si vous avez une contrainte, ou un élément qui nous aide à sélectionner votre proposition, comme une vidéo, des slides, n'hésitez pas à les ajouter ici. Ce champ ne sera visible que par les organisateurs. Si vous avez des contraintes, vous pouvez les indiquer ici. Si vous souhaitez envoyer un message à l’auteur de la proposition, saisissez-le ci-dessous. N’oubliez pas de spécifier à quelle proposition d’intervention votre message fait référence. Informations Information sur la propositon d’intervention Information à destination de l’auteur de la proposition Type d’intervention Orateur invité Type d’intervention : Étiquette Label dans le xml du programme Langue : LinkedIn LinkedIn : Se connecter Déconnexion Mastodon Mastodon : Supports Supports et vidéo Message de %(author)s Message envoyé ! Message aux organisateurs Messagerie Modification enregistrée avec succès. Mon profil Nom Nouvelle activité Nouvelle catégorie Nouvelle proposition Les nouveaux membres du staff seront informés de leur nouveau rôle par courrier électronique. Nouvelle étiquette Aucune activité. Pas de bibliographie. Aucune catégorie. Aucune description fournie. Aucun message. Aucune note. Aucune proposition. Aucune salle. Aucun orateur. Aucune étiquette. Aucun exposé Aucun exposé. Aucune session. Non, annuler Pas encore assignée Notes Notes : Notifier par e-mail ? Ok, pas de soucis ! Organisation Participer Changement de mot de passe Décision en attente En cours, score : %(score).1f Téléphone Numéro de téléphone Numéro de téléphone : Merci de confirmer votre participation : Merci de corriger ces erreurs. Veuillez vous connecter pour accéder à l’espace organisateurs. Veuillez sélectionner une catégorie. Veuillez sélectionner un ou plusieurs intervenants. Veuillez sélectionner un ou plusieurs exposés. Veuillez sélectionner un ou plusieurs bénévoles. Veuillez écrire votre e-mail ci-dessous : Propulsé par Aperçu Profil Profil modifié avec succès. Assigner à une salle Refusé Supprimer Adresse de réponse Réinitialiser son mot de passe Relecture en cours, nous vous tenons informé par e-mail. Salle Salles SMS préférés Envoyer Programme Programme supprimé du cache. Date de publication du programme URL de redirection du programme Programmé Exposés planifiés Lien secret : Domaine sécurisé (HTTPS) Envoyer Envoyer un message Envoyer un message – <em>ce message sera reçu par le participant et l’équipe d’organisation</em> Envoyer un e-mail Envoyer un e-mail à chaque intervenant Envoyer un e-mail à chaque intervenant de chaque exposé Envoyer un e-mail à chaque bénévoles Envoyer ! Afficher les options de filtrage… Afficher l’étiquette sur le programme public Afficher l’étiquette sur le programme organisateur Certains exposés ne sont pas encore planifiés. Désolé, j’ai un contretemps Désolé, je dois annuler. Désolé, j’ai un contre temps :-( Désolé, refusé :-( Désolé, l’appel à participation est fermé ! Désolé, nous ne connaissons pas cette e-mail. L’orateur %(speaker)s a %(action)s sa participation pour %(talk)s. Confirmation de l’orateur Orateurs Staff Espace organisateurs Membres du staff Statut Sujet Sujet : Envoyer Soumettre une proposition Syntaxe Étiquette Étiquettes Catégorie de proposition Titre de la proposition Nombre d’exposé Exposés Merci pour votre participation ! Merci pour votre participation ! Vous pouvez maintenant vous inscrire à une ou plusieurs activités. L’adresse de réponse doit être une chaine de texte formatable avec un argument « token » (e.g. ponyconf+{token}@exemple.com). L’orateur a annulé La confirmation de l’orateur a été notée. L’orateur a confirmé L’indisponibilité de l’intervenant a été notée. L’exposé a été %(action)s. L’exposé a été confirmé. Il y a une erreur dans le gabarit du corps. Il y a une erreur dans le gabarit du sujet. Cette description sera visible sur le programme. Ce champ est uniquement visible par les organisateurs. Le bénévole s'est proposé pour les activités suivantes : Le bénévole ne s’est proposé pour aucune activité. Créneau Titre Titre : Pour voir un aperçu de votre e-mail, cliquez sur une combinaison d'intervenant et exposé : Pour voir un aperçu de votre e-mail, cliquez sur un intervenant : Pour voir un aperçu de votre e-mail, cliquez sur un intervenant : Pour recevoir un e-mail avec un lien pour accéder à votre profil, veuillez entrer votre adresse mail ci-dessous. Pour voir les variables d’environnement disponibles, veuillez cliquer sur un intervenant. Pour voir les variables d’environnement disponibles, veuillez cliquer sur une combinaison d’intervenant et exposé. Pour voir les variables d’environnement disponibles, veuillez cliquer sur un bénévole. Total : Session Sessions Twitter Twitter : Exposés non planifiés Utilisateur créé avec succès. VIP Informations sur le lieu Vidéo Licence vidéo Date de publication des vidéos Visible par les orateurs Bénévole Bénévoles Date de fermeture de l’appel à bénévole Date d’ouverture de l’appel à bénévole Vote À voté Vote mis à jour En attente En attente de confirmation Nous avons enregistré votre indisponibilité. Nous avons besoin de vous ! Pour participer, veuillez indiquer votre nom et votre adresse e-mail et sélectionner les activités qui vous intéresses. Site web Website : Bienvenue <b>%(name)s</b> ! Bienvenue ! Oui, supprimer Vous avez déjà annulé votre participation à cet exposé. Vous avez déjà confirmé votre participation à cet exposé. Vous êtes prêt pour envoyer %(count)d e-mails. Vous pouvez utiliser le langage de gabarit <a href="http://jinja.pocoo.org/docs/2.10/">Jinja2</a>. Vous pouvez utiliser ce champ pour partager les supports de votre intervention. Vous avez un e-mail en attente d’envoi. Pour continuer son édition, cliquez <a href="%(email_url)s">ici</a>. Vous souhaitez peut-être ajouter un des intervenants suivants : Votre Nom Vos informations Votre participation a été prise en compte, merci ! Vos propositions Votre proposition a été transmise avec succès ! Vos gabarits ne compile pas avec (au moins) l’intervenant « %(speaker)s ». Vos gabarits ne compile pas avec (au moins) l’exposé « %(talk)s » et l’intervenant « %(speaker)s ». Vos gabarits ne compile pas avec (au moins) le bénévole « %(volunteer)s ». [%(conference)s] %(speaker)s a %(action)s sa participation [%(conference)s] Conversation avec %(user)s [%(conference)s] Message du staff [%(conference)s] Nouveau commentaire sur « %(talk)s » [%(conference)s] Quelqu’un a demandé à accéder à votre profil [%(conference)s] Merci pour votre aide ! [%(conference)s] Merci pour votre proposition « %(talk)s » [%(conference)s] L’exposé « %(talk)s » a été %(action)s [%(conference)s] L’exposé « %(talk)s » a été %(action)s. [%(conference)s] Vous avez été ajouté comme co-intervenant pour « %(talk)s » [%(conference)s] Votre exposé « %(talk)s » a été %(action)s [{}] Vous avez été ajouté aux membres du staff accepté accepté : %(accepted)s acceptés : %(accepted)s Aucune et moyenne : par annulé confirmé contacter par e-mail décliné télécharger télécharger au format CSV dans la session responsable non défini un pour chaque couple intervenant / exposé uniquement un par intervenant en attente : %(pending)s en attente : %(pending)s non fourni place refusé : %(refused)s refusés : %(refused)s supprimer non définie orateur aucune exposé bénévole vote avec vous devez confirmer votre participation vous ! 